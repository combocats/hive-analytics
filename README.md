**Please see the main repository for details:** https://bitbucket.org/combocats/hive-deploy

# Hive-analytics

This server uses Redis analytics queue, reads it and inserts rows in
specified Postgresql tables. Also provides interfaces for fetching data.

# Tests

To run tests use commands:

 + `make unit` - run all unit tests

# Setup database

If you make initial setup, you need to bootstrap the database.
For this case run command:

 `psql -f db_dump.sql -U [the_name_of_user]`

File `db_dump.sql` is in the root dir of the project. It will bootstrap empty
Postgres database with all the tables.

The name of the user is the name of postgres user (like `postgres` - default user in
any postgres db).

---
** Be careful! **

Do not use this sql script if you have running database, it will break everything.
Do not use this sql script if you need to ADD one more table into the database.
Remove this script right after you use it.

---