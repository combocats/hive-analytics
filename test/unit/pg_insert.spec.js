process.env.NODE_ENV = 'unittest';
var should  = require('should');
var _       = require('lodash');

var Modules = require('../modules');

describe('lib.pg_insert', function() {

    describe('constructor', function() {

        var PGInsert = require(Modules['pg_insert']);
        it('should properly create PGInsert instance', function() {
            var reader = new PGInsert({
                client: {},
                tableName: 'Test',
                columns: ['id', 'created']
            });
            reader.should.be.instanceOf(PGInsert);
            reader.insert.should.be.type('function');
            reader.columnsString.should.be.equal("id, created");
            reader.query.should.be.equal('INSERT INTO "Test"(id, created) VALUES ($1, $2)');
        });

        it('should throw error on create PGInsert instance with wrong options', function() {
            (function() {
                new PGInsert({
                    tableName: 'Test',
                    columns: ['id', 'created']
                });
            }).should.throw();
        });

    });

    describe('insert', function() {

        var PGInsert = require(Modules['pg_insert']);
        it('should call query method with specified params', function(done) {
            var reader = new PGInsert({
                client: {
                    query: function(data) {
                        data.should.have.properties(['name', 'text', 'values']);
                        data.values.should.eql(['1', '1']);
                        done();
                    }
                },
                tableName: 'Test',
                columns: ['id', 'created']
            });

            reader.insert({id: '1', created: '1'});
        });

    });

});