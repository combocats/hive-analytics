process.env.NODE_ENV = 'unittest';
var should  = require('should');
var _       = require('lodash');

var Modules = require('../modules');

describe('lib.queue_reader', function() {

    describe('constructor', function() {

        var QueueReader = require(Modules['queue_reader']);
        it('should properly create QueueReader instance', function() {
            var reader = new QueueReader({
                key: 'test',
                onRead: function() {},
                client: {}
            });
            reader.should.be.instanceOf(QueueReader);
            reader.start.should.be.type('function');
        });

        it('should throw error on attempt to create instance without key option', function() {
            (function() {
                new QueueReader({
                    onRead: function() {},
                    client: {}
                });
            }).should.throw();
        });

        it('should throw error on attempt to create instance without onRead option', function() {
            (function() {
                new QueueReader({
                    key: 'test',
                    client: {}
                });
            }).should.throw();
        });

        it('should throw error on attempt to create instance with nonfunction onRead', function() {
            (function() {
                new QueueReader({
                    key: 'test',
                    onRead: {},
                    client: {}
                });
            }).should.throw();
        });

    });

    describe('start', function() {

        var QueueReader = require(Modules['queue_reader']);
        it('should properly starts reading queue', function(done) {
            var client = {
                LPOP: function(key) {
                    _.last(arguments)(null, '{"ok": true}');
                }
            };
            var reader = new QueueReader({
                key: 'test',
                onRead: function(data) {
                    data.should.eql({ok: true});
                    done();
                },
                client: client
            });
            reader.next = function() {};
            reader.start();
        });

        it('should call next if queue is empty', function(done) {
            var client = {
                LPOP: function(key) {
                    _.last(arguments)(null, null);
                }
            };
            var reader = new QueueReader({
                key: 'test',
                onRead: function(data) {},
                client: client
            });
            reader.next = function() {
                true.should.be.ok;
                done();
            };
            reader.start();
        });

    });

});