var path = require('path');
var _    = require('lodash');

var Modules = {
    'queue_reader'      : './lib/queue_reader',
    'pg_insert'         : './lib/pg_insert',
    'activity.utils'    : './lib/activity/utils',
    'activity.reporter' : './lib/activity/reporter',
    'activity.tracker'  : './lib/activity/tracker'
};

var absolutPaths = {};
_.forEach(Modules, function(value, key) {
    absolutPaths[key] = path.resolve(value);
});

module.exports = absolutPaths;