process.env.NODE_ENV = 'test';
var exec     = require('child_process').exec;
var spawn    = require('child_process').spawn;
var request  = require('request');
var util     = require('util');
var settings = require('hive-common').settings;
var _        = require('lodash');
var fs       = require('fs');
var async    = require('async');
var redis    = require('redis');
var client   = redis.createClient(settings.config.redis_port, settings.config.redis_host);
var testUsers = require('./test_users');
var appProcess;

function line() { var line = ''; while(line.length < 50){ line+='*'} return line;}

console.log(line(), '\n* PREPARE TEST ENVIRONMENT');

function dropPostgresDB(callback) {
    exec(util.format('psql -U %s -c "SELECT pg_terminate_backend(pg_stat_activity.pid) ' +
        'FROM pg_stat_activity WHERE pg_stat_activity.datname = \'%s\' ' +
        'AND pid <> pg_backend_pid();"', settings.config.pg_user, settings.config.pg_db), function(err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);

        exec(util.format('dropdb "%s" -U %s', settings.config.pg_db, settings.config.pg_user), function(err, stdout, stderr) {
            console.log(stdout);
            console.log(stderr);
            callback(err);
        });
    });
}

function createPostgresDB(callback) {
    exec(util.format('psql -U %s -f %s', settings.config.pg_user, 'test/db_dump.sql'), function(err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        callback(err);
    });
}

function addUserSessionToRedis(callback) {
    var multi = client.multi();
    for(var id in testUsers) {
        var session = {
            cookie: {
                expires: new Date() + 1000*60*60*24,
                httpOnly: true,
                originalMaxAge: 86400000,
                path: "/"
            },
            passport: {
                user: id
            }
        };
        multi.set("sess:" + id, JSON.stringify(session));
    }
    multi.exec(function(err) {
        callback(err);
    });
}

function removeUserSessionFromRedis(callback) {
    var multi = client.multi();
    for(var id in testUsers) {
        multi.del("sess:" + id);
    }
    multi.exec(function(err) {
        callback(err);
    });
}

function clearQueue(callback) {
    var multi = client.multi();
    client.keys(settings.config.queue_prefix + "*", function(err, keys) {
        console.log("* queue keys: ", keys);
        for(var i in keys) {
            multi.del(keys[i]);
        }
        multi.exec(function(err) {
            callback(err);
        });
    });
}

function killProcess() {
    if (appProcess) {
        console.log("* kill app process");
        console.log("* END UP TEST SESSION\n", line());
        appProcess.kill();
        appProcess = null;
    }
}

var before = function(done) {
    this.timeout(10000);

    try {
        fs.lstatSync("./logs");
    } catch(err) {
        if(err.code == 'ENOENT') {
            console.log("* ./logs dir was created for test logs");
            fs.mkdirSync("./logs");
        }
    }

    appProcess = spawn('node', ['app.js'], {env: _.extend(process.env, { NODE_ENV: 'test' })});

    var noWhere = function(chunk) {
//        console.log(chunk.toString());
    };
    appProcess.stdout.on('data', noWhere);
    appProcess.stderr.on('data', noWhere);

    appProcess.on('exit', function (e) {
        console.log('\n\nanalytics server terminated', e);
        console.log('\n\n');
    });

    /**
     * Kill the appProcess after test finished
     */
    process.on('exit', killProcess);

    /**
     * Waiting for server and drop db
     */
    function waitForServer(callback) {
        var url = util.format("http://localhost:%d/check", settings.config.port);
        var interval = setInterval(function() {
            request(url, function(err, res, body) {
                console.log("* * checking server ...");
                if(res && res.statusCode == 200) {
                    console.log('* now server is running\n', line(), '\n\n\n');
                    clearInterval(interval);
                    callback();
                }
            });
        }, 500);
    }

    async.waterfall([
        function(cb) { dropPostgresDB(cb); },
        function(cb) { createPostgresDB(cb); },
        function(cb) { clearQueue(cb); },
        function(cb) { addUserSessionToRedis(cb) },
        function(cb) { waitForServer(cb); }
    ], done);
};

var after = function(done) {
    async.waterfall([
        function(cb) { clearQueue(cb); },
        function(cb) { removeUserSessionFromRedis(cb) },
        function(cb) { killProcess(); cb(); }
    ], done);
};

module.exports = {
    before: before,
    after: after
};