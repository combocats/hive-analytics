var hooks   = require('../e2e_hooks');
var _       = require('lodash');
var util    = require('util');
require('should');
var Modules = require('../modules');

var testUsers   = require('../test_users');
var activeUser  = testUsers[Object.keys(testUsers)[0]];
var pg = require('pg');

var config = require('hive-common').settings.config;

describe('lib.readers.users', function() {

    before(hooks.before);
    after(hooks.after);

    describe('users', function() {

        it('should read from redis and insert into postgres', function(done) {
            var redis   = require('redis');
            var client  = redis.createClient(config.redis_port, config.redis_host);
            client.RPUSH(config.queue_prefix + config.queue_names.users, JSON.stringify({
                id: activeUser._id,
                created: new Date(),
                country: 'RU',
                channel: 'fb'
            }));
            _.delay(function() {
                pg.connect({
                    user: config.pg_user,
                    password: config.pg_passwd,
                    host: config.pg_host,
                    database: config.pg_db
                }, function(err, client) {
                    client.query('SELECT * FROM "Users"', function(err, result) {
                        result.rows.length.should.be.equal(1);
                        done();
                    });
                });
            }, 200);
        });

    });

});