var hooks   = require('../e2e_hooks');
var _       = require('lodash');
var util    = require('util');
var moment  = require('moment');
var chokidar = require('chokidar');
var fs = require('fs');
require('should');

var testUsers   = require('../test_users');
var activeUser  = testUsers[Object.keys(testUsers)[0]];
var pg = require('pg');

var config = require('hive-common').settings.config;

describe.only('lib.dump.traps.sessions', function() {

    before(hooks.before);
    after(hooks.after);

    describe('users', function() {

        it('should add a user to Users table', function() {
            var redis   = require('redis');
            var client  = redis.createClient(config.redis_port, config.redis_host);
            client.RPUSH(config.queue_prefix + config.queue_names.users, JSON.stringify({
                id: activeUser._id,
                created: new Date(),
                country: 'RU',
                channel: 'fb'
            }));
        });

    });

    describe('readers.traps_sessions', function() {

        it('should add a session to the queue', function() {
            var redis   = require('redis');
            var client  = redis.createClient(config.redis_port, config.redis_host);

            var start_1 = moment([2013, 11, 11, 11]);
            var end_1 = moment([2013, 11, 11, 11]).add(1000*60*10);
            var start_2 = moment([2013, 11, 11, 20]);
            var end_2 = moment([2013, 11, 11, 20]).add(1000*60*15);

            client.RPUSH(config.queue_prefix + config.queue_names.traps.sessions, JSON.stringify({
                id: activeUser._id,
                session_start: start_1.toDate(),
                session_end: end_1.toDate(),
                platform: 'iPhone'
            }), JSON.stringify({
                id: activeUser._id,
                session_start: start_2.toDate(),
                session_end: end_2.toDate(),
                platform: 'iPhone'
            })
            );
        });

    });

    describe('dump.traps_sessions', function() {

        it('should create dump of TrapsSessions table', function(done) {
            this.timeout(10000);
            var watcher = chokidar.watch('./dump');

            watcher.on('add', function(path) {
                fs.readFile(path, function (err, data) {
                    var dump = data.toString();
                    dump.length.should.be.within(180, 200);
                    fs.unlink(path);
                });
                _.delay(done, 100);
            });
        });

    });

});