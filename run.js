var cluster = require('cluster');

if(process.env.NODE_ENV == "prod") {
    var numCPUs = require('os').cpus().length;
    console.log("  num of CPUs: ", numCPUs);
} else {
    numCPUs = 1;
}

if (cluster.isMaster) {
    console.log("Master have just started");
    for (i = 1; i <= numCPUs; i++) {
        cluster.fork();
    }
} else {
    require('./app.js');
}