var log         = require('hive-common').logger('readers/traps_sessions');
var config      = require('hive-common').settings.config;
var QueueReader = require('../queue_reader');
var PGInsert    = require('../pg_insert');

module.exports = function(redisClient, pgConnection) {
    // add reader for Traps sessions queue
    pgConnection.then(function(client) {
        // create insert function for TrapsSessions table
        var usersInsert = new PGInsert({
            client: client,
            tableName: config.pg_tables.traps.sessions,
            columns: ['id', 'session_start', 'session_end', 'platform']
        });

        // create reader for Traps Session queue
        var trapsSessionReader = new QueueReader({
            client: redisClient,
            key: config.queue_prefix + config.queue_names.traps.sessions,
            onRead: function(data) {
                log.debug("traps sessions onRead called: ", data);
                usersInsert.insert(data).then(function(result) {
                    log.debug("successfully insert row to TrapsSessions: ", result);
                });
            }
        });

        trapsSessionReader.start();
    });
};