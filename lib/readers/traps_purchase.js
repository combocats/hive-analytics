var log         = require('hive-common').logger('readers/traps_purchase');
var config      = require('hive-common').settings.config;
var QueueReader = require('../queue_reader');
var PGInsert    = require('../pg_insert');

module.exports = function(redisClient, pgConnection) {
    // add reader for Traps purchase queue
    pgConnection.then(function(client) {
        // create insert function for TrapsPurchase table
        var usersInsert = new PGInsert({
            client: client,
            tableName: config.pg_tables.traps.purchase,
            columns: ['id', 'timestamp', 'amount', 'purchase_id', 'type']
        });

        // create reader for Traps purchase queue
        var trapsPurchaseReader = new QueueReader({
            client: redisClient,
            key: config.queue_prefix + config.queue_names.traps.purchase,
            onRead: function(data) {
                log.debug("traps purchase onRead called: ", data);
                usersInsert.insert(data).then(function(result) {
                    log.debug("successfully insert row to TrapsPurchase: ", result);
                });
            }
        });

        trapsPurchaseReader.start();
    });
};