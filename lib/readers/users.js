var log         = require('hive-common').logger('readers/users');
var config      = require('hive-common').settings.config;
var QueueReader = require('../queue_reader');
var PGInsert    = require('../pg_insert');

module.exports = function(redisClient, pgConnection) {
    // add reader for Users queue
    pgConnection.then(function(client) {
        // create insert function for Users table
        var usersInsert = new PGInsert({
            client: client,
            tableName: 'Users',
            columns: ['id', 'created', 'country', 'channel']
        });

        // create reader for Users queue
        var usersReader = new QueueReader({
            client: redisClient,
            key: config.queue_prefix + config.queue_names.users,
            onRead: function(data) {
                log.debug("usersReader onRead called: ", data);
                usersInsert.insert(data).then(function(result) {
                    log.debug("successfully insert row to Users: ", result);
                });
            }
        });

        usersReader.start();
    });
};