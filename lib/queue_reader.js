var Q        = require('q');
var log      = require('hive-common').logger('queue_reader');
var config   = require('hive-common').settings.config;
var _        = require('lodash');
var should   = require('should');

var errors   = require('./errors');

var QueueReader = function(options) {
    options.should.be.ok;
    options.should.have.properties(['key', 'onRead', 'client']);
    options.onRead.should.be.type('function');
    _.extend(this, options);
};

/**
 * Next iteration starter function.
 */
QueueReader.prototype.next = function() {
    this.start();
};

/**
 * Starts queue reader.
 * @returns {promise.then|*}
 */
QueueReader.prototype.start = function() {
    var self = this;

    return Q(1)
        .then(function() {
            return self.getNext()
                .catch(function() {
                    log.warn("no items in the queue: ", self.key);
                    _.delay(_.bind(self.next, self), config.timeout_for_empty_queue);
                    return Q.reject();
                });
        })
        .then(function(data) {
            // start the action specified as onRead
            self.onRead(data);
            self.next();
        });
};

/**
 * Get next item from queue and parse it.
 * @returns {promise.then|*}
 */
QueueReader.prototype.getNext = function() {
    var self = this;
    return Q.ninvoke(self.client, 'LPOP', self.key)
        .then(function(data) {
            if(data) {
                try {
                    data = JSON.parse(data);
                    return data;
                } catch (err) {
                    return Q.reject(errors.COULD_NOT_PARSE_DOC_FROM_CACHE({error: err}));
                }
            } else {
                return Q.reject(errors.COULD_NOT_READ_DATA_FROM_QUEUE({key: self.key}));
            }
        });
};

module.exports = QueueReader;