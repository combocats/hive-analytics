var log         = require('hive-common').logger('dump/traps_purchase');
var config      = require('hive-common').settings.config;
var CronJob     = require('cron').CronJob;
var PGDump      = require('../pg_dump');

module.exports = function(pgConnection) {

    pgConnection.then(function(client) {
        // create dumper for traps_purchase
        var TrapsPurchaseDumper = new PGDump({
            client: client,
            tableName: config.pg_tables.traps.purchase,
            field: 'timestamp'
        });

        var job = new CronJob(config.dump_interval, function(){
            log.info('start dump traps purchase');
            TrapsPurchaseDumper.dump(new Date());
        }, null, true, "America/Los_Angeles");
    });
};