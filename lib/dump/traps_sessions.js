var log         = require('hive-common').logger('dump/traps_sessions');
var config      = require('hive-common').settings.config;
var CronJob     = require('cron').CronJob;
var PGDump      = require('../pg_dump');

module.exports = function(pgConnection) {

    pgConnection.then(function(client) {
        // create dumper for traps_session
        var TrapsSessionDumper = new PGDump({
            client: client,
            tableName: config.pg_tables.traps.sessions,
            field: 'session_end'
        });

        var job = new CronJob(config.dump_interval, function(){
            log.info('start dump traps sessions');
            TrapsSessionDumper.dump(new Date());
        }, null, true, "America/Los_Angeles");
    });
};