var Q  = require('q');
var pg = require('pg');

var PGConnect = {};

/**
 * Creates a connection to the DB. Returns promise with client
 * object when connection established.
 * @param {string} conString - URL of the postgres db.
 * @returns {promise}
 */
PGConnect.connect = function(conObj) {
    var defer = Q.defer();

    pg.connect(conObj, function(err, client, done) {
        if(err) {
            return defer.reject(err);
        } else {
            return defer.resolve(client);
        }
    });

    return defer.promise;
};

module.exports = PGConnect;