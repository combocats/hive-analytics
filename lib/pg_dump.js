var log      = require('hive-common').logger('pg_dump');
var config   = require('hive-common').settings.config;
var moment   = require('moment');
var _        = require('lodash');
var should   = require('should');
var util     = require('util');
var Q        = require('q');
var path     = require('path');
var fs       = require('fs');

var errors   = require('./errors');

/**
 * Creates and bootstrap new dumper. As each table has its own condition for dumping
 * you need to specify the name of the table and field as options.
 * @param options
 * @constructor
 */
var PGDump = function(options) {
    options.should.be.ok;
    options.should.have.properties(['client', 'tableName', 'field']);

    options.tableName.should.be.type('string');
    options.field.should.be.type('string');

    options.copyQuery = "COPY (" +
        "SELECT * FROM \"%s\" " +
        "WHERE \"%s\" < '%s'" +
        ") TO stdin WITH CSV HEADER;";

    options.deleteQuery = util.format("DELETE FROM \"%s\" WHERE \"%s\" < $1", options.tableName, options.field);

    options.rollback = function(client) {
        client.query('ROLLBACK', function() {
            log.warn('Dump operation rollback called!', options.tableName);
        });
    };

    _.extend(this, options);
};

PGDump.prototype.dumpFileName = function(date) {
    var dir = path.resolve(config.pg_dump_dir);
    log.debug("dumpFileName: ", dir);
    return dir + moment(date).format('/YYYY-MM-DD-') + this.tableName + '.csv';
};

/**
 * Dumps rows of specified table to dump directory.
 * Will create CSV file with table and timestamp as parts of the name.
 * @param {Date} now - The date from where to dump/crop rows.
 */
PGDump.prototype.dump = function(now) {
    log.info("Dump table method called: ", this.tableName, now);
    var self = this;
    var client = self.client;

    var date = moment(now).subtract('days', config.dump_depth);

    client.query('BEGIN', function(err, result) {
        log.info("Begin transaction", err);
        if(err) return self.rollback(client);
        var file = self.dumpFileName(date);
        var command = util.format(self.copyQuery, self.tableName, self.field, date.format());
        log.debug("COPY command: ", command);

        var stream = client.copyTo(command);
        var writable = fs.createWriteStream(file);

        stream.pipe(writable);

        stream.on('end', function() {
            log.info("close writable stream");
            writable.end();

            client.query(self.deleteQuery, [date], function(err, result) {
                log.info("Delete dumped rows", err);
                if(err) return self.rollback(client);
                client.query('COMMIT');
            });
        });
    });
};

module.exports = PGDump;