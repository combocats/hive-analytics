var errors = require('hive-common').errors;

var CacheErrors = {
    COULD_NOT_READ_DATA_FROM_QUEUE: {
        status: 500,
        code: 100,
        message: 'Could not read data from redis queue'
    },
    COULD_NOT_PARSE_DOC_FROM_CACHE: {
        status: 500,
        code: 101,
        message: 'Could not parse JSON from redis cache'
    }
};

module.exports = errors(
    CacheErrors
);