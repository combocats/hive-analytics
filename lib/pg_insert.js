var log     = require('hive-common').logger('pg_insert');
var _        = require('lodash');
var should   = require('should');
var util     = require('util');
var Q        = require('q');

var errors   = require('./errors');

/**
 * Creates and bootstrap new inserter. Predefines query string, that will
 * be filled with some data while insert() method called.
 * @param options
 * @constructor
 */
var PGInsert = function(options) {
    options.should.be.ok;
    options.should.have.properties(['client', 'columns', 'tableName']);

    // columnsString is the sequence of columns names like "id, country, created"
    if(!_.isArray(options.columns)) throw new Error('columns should be Array');
    options.columnsString = options.columns.join(', ');

    // placeholders is the sequence like "$1, $2, $3" that will be replaced with values
    // while the query is processed
    var placeholders = [];
    _.forEach(options.columns, function(value, index) {
        placeholders.push('$' + (index + 1));
    });
    options.placeholders = placeholders.join(', ');

    // prepopulated postgres query
    options.query = util.format('INSERT INTO "%s"(%s) VALUES (%s)',
        options.tableName, options.columnsString, options.placeholders);

    // query name for using prepared statements in postgres
    options.queryName = util.format('insert into %s', options.tableName);

    _.extend(this, options);
};

/**
 * Inserts specified data into the table.
 * @param {object} data - Data that should be inserted into the table.
 * @returns {object} Postgres query object
 */
PGInsert.prototype.insert = function(data) {
    var self = this;

    var values = [];
    _.forEach(self.columns, function(key) {
        values.push(data[key]);
    });

    return Q.ninvoke(self.client, 'query', {
        name: self.queryName,
        text: self.query,
        values: values
    }).catch(function(err) {
        log.error("error on insert to postgres: ", self.queryName, err);
        return Q.reject(err);
    });
};

module.exports = PGInsert;