REPORTER = spec

unit:
		@./node_modules/.bin/mocha \
			--reporter $(REPORTER) \
			--ui bdd \
			--recursive \
			test/unit/

e2e:
ifdef ONLY
		@./node_modules/.bin/mocha \
			--reporter $(REPORTER) \
			--ui bdd \
			--recursive \
			test/e2e/$(ONLY)
else
		@./node_modules/.bin/mocha \
			--reporter $(REPORTER) \
			--ui bdd \
			--recursive \
			test/e2e
endif

all: unit e2e

.PHONY: all