var log     = require('hive-common').logger('app');
var util    = require('util');
var config  = require('hive-common').settings.config;

var _       = require('lodash');

var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');

//----- Create Redis and Postgres connections ------//

var redis   = require('redis');
var redisClient  = redis.createClient(config.redis_port, config.redis_host);

var PGConnect = require('./lib/pg_connect');
var pgConnection = PGConnect.connect({
    user: config.pg_user,
    password: config.pg_passwd,
    host: config.pg_host,
    database: config.pg_db
});


//----- Setup all queue readers -------------------//

require('./lib/readers/users')(redisClient, pgConnection);
require('./lib/readers/traps_sessions')(redisClient, pgConnection);

//----- Setup all dump  -------------------//

require('./lib/dump/traps_sessions')(pgConnection);

//----- Setup express app --------//

var app = express();

app.use(bodyParser());
app.use(cookieParser());

/* Config session storage */
var RedisStore = require('connect-redis')(session);
var sessionClient = redis.createClient(config.redis_port, config.redis_host);

var sessionConf = {
    secret: 'hive secret cat',
    store: new RedisStore({client: sessionClient})
};

//CORS middleware for
app.use(require('hive-common').cors);

// token based authorization for CORS and non-cookie agents
app.use(require('hive-common').tokenAccess);

app.use(session(sessionConf));

/**
 * End point for all error requests to public API
 */
app.use('/api', require('hive-common').endPoints.error);

/**
 * End point for all non error requests to api
 */
app.use('/api', require('hive-common').endPoints.success);

/**
 * Check api for availability tests.
 */
app.get('/check', function(req, res) {
    res.json({ok: true});
});

var server = require('http').createServer(app);
server.listen(config.port, config.host);

log.info("listen on %s", config.port);
log.info("main analytics server started ", process.pid);