/**
 * Example configurations for development and for production.
 * This config file will be changed while deploying, so use it
 * just like an example or doc.
 */
module.exports = {
    dev: {
        port: '4006',
        host: 'localhost',
        redis_port: '6379',
        redis_host: 'localhost',
        // Redis queue settings
        queue_prefix: 'analytics:',
        queue_names: {
            users: 'users',
            traps: {
                sessions: 'traps:sessions',
                purchase: 'traps:purchase'
            }
        },
        timeout_for_empty_queue: 1000,
        // postgres data
        pg_user: 'postgres',
        pg_passwd: null,
        pg_host: 'localhost',
        pg_db: 'HiveAnalyticsTest',
        // postgres tables for analytics storage
        pg_tables: {
            users: 'Users',
            traps: {
                sessions: 'TrapsSessions',
                purchase: 'TrapsPurchase'
            }
        },
        // dump tables config
        pg_dump_dir: './dump',
        // should be at least one day
        dump_interval: '1 0 * * 0',
        dump_depth: 90 // days
    },
    prod: {

    },
    test: {
        port: '5006',
        host: 'localhost',
        redis_port: '6379',
        redis_host: 'localhost',
        // Redis queue settings
        queue_prefix: 'test:analytics:',
        queue_names: {
            users: 'users',
            traps: {
                sessions: 'traps:sessions',
                purchase: 'traps:purchase'
            }
        },
        timeout_for_empty_queue: 100,
        // postgres data
        pg_user: 'postgres',
        pg_passwd: null,
        pg_host: 'localhost',
        pg_db: 'HiveAnalyticsTest',
        // postgres tables for analytics storage
        pg_tables: {
            users: 'Users',
            traps: {
                sessions: 'TrapsSessions',
                purchase: 'TrapsPurchase'
            }
        },
        // dump tables config
        pg_dump_dir: './dump',
        dump_interval: '*/6 * * * * *',
        dump_depth: 1 // days
    },
    unittest: {
        port: '5006',
        host: 'localhost',
        redis_port: '6379',
        redis_host: 'localhost',
        // Redis queue settings
        queue_prefix: 'unit:analytics:',
        queue_names: {
            users: 'users',
            traps: {
                sessions: 'traps:sessions',
                purchase: 'traps:purchase'
            }
        },
        timeout_for_empty_queue: 1,
        // postgres data
        pg_user: 'postgres',
        pg_passwd: null,
        pg_host: 'localhost',
        pg_db: 'HiveAnalyticsTest',
        // postgres tables for analytics storage
        pg_tables: {
            users: 'Users',
            traps: {
                sessions: 'TrapsSessions',
                purchase: 'TrapsPurchase'
            }
        },
        // dump tables config
        pg_dump_dir: './dump/',
        dump_interval: '1 0 * * 0',
        dump_depth: 90 // days
    }
};